# Installation
> `npm install --save @types/ftp`

# Summary
This package contains type definitions for ftp (https://github.com/mscdex/node-ftp).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/ftp.

### Additional Details
 * Last updated: Sat, 23 Oct 2021 01:31:26 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Rogier Schouten](https://github.com/rogierschouten), [Nathan Rajlich](https://github.com/TooTallNate), and [Marvin Witt](https://github.com/NurMarvin).
